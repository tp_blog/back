<?php

namespace App\DataFixtures;

use App\Entity\Article;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class ArticleFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');
        // $product = new Product();
        // $manager->persist($product);

        $article1 = new Article();
        $article1->setTitle('Article1')
            ->setPicture('https://www.fnacspectacles.com/static/0/visuel/310/429/47TER-TOUR_4297087678870546068.jpg')
            ->setContent('Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed pulvinar malesuada sapien, vitae mattis libero feugiat vitae. Phasellus semper lorem sit amet varius facilisis. Vestibulum ac bibendum lacus. Duis at risus dui. Cras in viverra ex. Sed consequat, ligula sed rhoncus feugiat, velit nibh pretium ligula, non interdum purus enim eget quam. Etiam et dictum nisi. Donec quis orci libero. Praesent iaculis nunc urna, elementum feugiat diam maximus vitae. In eu nisl libero. Donec accumsan ac justo sed dictum. Morbi molestie ac arcu vitae commodo. Integer vel nunc lorem. Praesent eu feugiat tortor. Nunc malesuada quis augue ut sollicitudin. Suspendisse lacinia risus ac augue mollis, vitae ultricies purus facilisis.

            Curabitur accumsan enim ut lacus ornare, id ultrices quam maximus. Cras id fermentum odio. Suspendisse luctus maximus sapien, eu gravida erat vehicula sit amet. Suspendisse rhoncus ligula in metus posuere dapibus sit amet quis lorem. Nullam viverra ipsum eget felis consectetur vestibulum. Praesent sagittis porta convallis. Duis pretium vestibulum risus quis sollicitudin. Praesent quis dolor ornare, commodo metus in, venenatis nisl. Donec convallis porta convallis. Nunc aliquam venenatis elit at fringilla. Donec dapibus lobortis vestibulum. Nam ac lacus vitae nunc vulputate congue.
            
            Maecenas sed tortor quis mauris aliquet auctor. Aliquam malesuada nisl vel velit sollicitudin, vel finibus mi scelerisque. Vivamus porta semper laoreet. Integer urna turpis, elementum sit amet commodo ut, sollicitudin in nisl. Cras sed congue metus. Pellentesque varius libero eu arcu vestibulum, id commodo urna feugiat. Phasellus rhoncus in ex vel pretium. Aenean condimentum orci non facilisis faucibus. Sed condimentum velit ac mollis lobortis.
            
            Donec convallis non nibh at tincidunt. In sem urna, scelerisque sit amet erat nec, placerat accumsan sem. Maecenas pretium nisl erat, sed aliquam turpis sagittis et. Integer sodales quam ac odio placerat, nec mollis justo viverra. Pellentesque accumsan nibh in quam pretium, a euismod ex elementum. Donec ut malesuada dui. Sed at dapibus nulla. Donec et dui auctor, sodales mauris in, malesuada magna. Etiam et sem sit amet mi gravida sollicitudin pretium vulputate dui. Ut ac orci eget ipsum fringilla facilisis. Morbi ultrices tortor ac aliquet fringilla. Cras tempor posuere dolor. Pellentesque nulla odio, eleifend et neque a, iaculis facilisis nulla. Ut id leo a turpis fermentum fringilla vitae sit amet tortor. Vestibulum aliquam, ipsum a congue convallis, eros odio bibendum metus, quis ultrices nisi nunc at erat. In bibendum mauris ligula, quis lacinia turpis venenatis quis.')
            ->setCreationDate(new \DateTime('2021-05-22 20:00'))
            ->setPublicationDate(new \DateTime('2021-05-22 20:00'));
        $manager->persist($article1);

        $article2 = new Article();
        $article2->setTitle('Article2')
            ->setPicture('https://www.fnacspectacles.com/static/0/visuel/310/429/47TER-TOUR_4297087678870546068.jpg')
            ->setContent('Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed pulvinar malesuada sapien, vitae mattis libero feugiat vitae. Phasellus semper lorem sit amet varius facilisis. Vestibulum ac bibendum lacus. Duis at risus dui. Cras in viverra ex. Sed consequat, ligula sed rhoncus feugiat, velit nibh pretium ligula, non interdum purus enim eget quam. Etiam et dictum nisi. Donec quis orci libero. Praesent iaculis nunc urna, elementum feugiat diam maximus vitae. In eu nisl libero. Donec accumsan ac justo sed dictum. Morbi molestie ac arcu vitae commodo. Integer vel nunc lorem. Praesent eu feugiat tortor. Nunc malesuada quis augue ut sollicitudin. Suspendisse lacinia risus ac augue mollis, vitae ultricies purus facilisis.

            Curabitur accumsan enim ut lacus ornare, id ultrices quam maximus. Cras id fermentum odio. Suspendisse luctus maximus sapien, eu gravida erat vehicula sit amet. Suspendisse rhoncus ligula in metus posuere dapibus sit amet quis lorem. Nullam viverra ipsum eget felis consectetur vestibulum. Praesent sagittis porta convallis. Duis pretium vestibulum risus quis sollicitudin. Praesent quis dolor ornare, commodo metus in, venenatis nisl. Donec convallis porta convallis. Nunc aliquam venenatis elit at fringilla. Donec dapibus lobortis vestibulum. Nam ac lacus vitae nunc vulputate congue.
            
            Maecenas sed tortor quis mauris aliquet auctor. Aliquam malesuada nisl vel velit sollicitudin, vel finibus mi scelerisque. Vivamus porta semper laoreet. Integer urna turpis, elementum sit amet commodo ut, sollicitudin in nisl. Cras sed congue metus. Pellentesque varius libero eu arcu vestibulum, id commodo urna feugiat. Phasellus rhoncus in ex vel pretium. Aenean condimentum orci non facilisis faucibus. Sed condimentum velit ac mollis lobortis.
            
            Donec convallis non nibh at tincidunt. In sem urna, scelerisque sit amet erat nec, placerat accumsan sem. Maecenas pretium nisl erat, sed aliquam turpis sagittis et. Integer sodales quam ac odio placerat, nec mollis justo viverra. Pellentesque accumsan nibh in quam pretium, a euismod ex elementum. Donec ut malesuada dui. Sed at dapibus nulla. Donec et dui auctor, sodales mauris in, malesuada magna. Etiam et sem sit amet mi gravida sollicitudin pretium vulputate dui. Ut ac orci eget ipsum fringilla facilisis. Morbi ultrices tortor ac aliquet fringilla. Cras tempor posuere dolor. Pellentesque nulla odio, eleifend et neque a, iaculis facilisis nulla. Ut id leo a turpis fermentum fringilla vitae sit amet tortor. Vestibulum aliquam, ipsum a congue convallis, eros odio bibendum metus, quis ultrices nisi nunc at erat. In bibendum mauris ligula, quis lacinia turpis venenatis quis.')
            ->setCreationDate(new \DateTime('2021-05-22 20:00'))
            ->setPublicationDate(new \DateTime('2021-05-22 20:00'));
        $manager->persist($article2);

        $article3 = new Article();
        $article3->setTitle('Article3')
            ->setPicture('https://www.fnacspectacles.com/static/0/visuel/310/429/47TER-TOUR_4297087678870546068.jpg')
            ->setContent('Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed pulvinar malesuada sapien, vitae mattis libero feugiat vitae. Phasellus semper lorem sit amet varius facilisis. Vestibulum ac bibendum lacus. Duis at risus dui. Cras in viverra ex. Sed consequat, ligula sed rhoncus feugiat, velit nibh pretium ligula, non interdum purus enim eget quam. Etiam et dictum nisi. Donec quis orci libero. Praesent iaculis nunc urna, elementum feugiat diam maximus vitae. In eu nisl libero. Donec accumsan ac justo sed dictum. Morbi molestie ac arcu vitae commodo. Integer vel nunc lorem. Praesent eu feugiat tortor. Nunc malesuada quis augue ut sollicitudin. Suspendisse lacinia risus ac augue mollis, vitae ultricies purus facilisis.

            Curabitur accumsan enim ut lacus ornare, id ultrices quam maximus. Cras id fermentum odio. Suspendisse luctus maximus sapien, eu gravida erat vehicula sit amet. Suspendisse rhoncus ligula in metus posuere dapibus sit amet quis lorem. Nullam viverra ipsum eget felis consectetur vestibulum. Praesent sagittis porta convallis. Duis pretium vestibulum risus quis sollicitudin. Praesent quis dolor ornare, commodo metus in, venenatis nisl. Donec convallis porta convallis. Nunc aliquam venenatis elit at fringilla. Donec dapibus lobortis vestibulum. Nam ac lacus vitae nunc vulputate congue.
            
            Maecenas sed tortor quis mauris aliquet auctor. Aliquam malesuada nisl vel velit sollicitudin, vel finibus mi scelerisque. Vivamus porta semper laoreet. Integer urna turpis, elementum sit amet commodo ut, sollicitudin in nisl. Cras sed congue metus. Pellentesque varius libero eu arcu vestibulum, id commodo urna feugiat. Phasellus rhoncus in ex vel pretium. Aenean condimentum orci non facilisis faucibus. Sed condimentum velit ac mollis lobortis.
            
            Donec convallis non nibh at tincidunt. In sem urna, scelerisque sit amet erat nec, placerat accumsan sem. Maecenas pretium nisl erat, sed aliquam turpis sagittis et. Integer sodales quam ac odio placerat, nec mollis justo viverra. Pellentesque accumsan nibh in quam pretium, a euismod ex elementum. Donec ut malesuada dui. Sed at dapibus nulla. Donec et dui auctor, sodales mauris in, malesuada magna. Etiam et sem sit amet mi gravida sollicitudin pretium vulputate dui. Ut ac orci eget ipsum fringilla facilisis. Morbi ultrices tortor ac aliquet fringilla. Cras tempor posuere dolor. Pellentesque nulla odio, eleifend et neque a, iaculis facilisis nulla. Ut id leo a turpis fermentum fringilla vitae sit amet tortor. Vestibulum aliquam, ipsum a congue convallis, eros odio bibendum metus, quis ultrices nisi nunc at erat. In bibendum mauris ligula, quis lacinia turpis venenatis quis.')
            ->setCreationDate(new \DateTime('2021-05-22 20:00'))
            ->setPublicationDate(new \DateTime('2021-05-22 20:00'));
        $manager->persist($article3);

        $article4 = new Article();
        $article4->setTitle('Article4')
            ->setPicture('https://www.fnacspectacles.com/static/0/visuel/310/429/47TER-TOUR_4297087678870546068.jpg')
            ->setContent('Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed pulvinar malesuada sapien, vitae mattis libero feugiat vitae. Phasellus semper lorem sit amet varius facilisis. Vestibulum ac bibendum lacus. Duis at risus dui. Cras in viverra ex. Sed consequat, ligula sed rhoncus feugiat, velit nibh pretium ligula, non interdum purus enim eget quam. Etiam et dictum nisi. Donec quis orci libero. Praesent iaculis nunc urna, elementum feugiat diam maximus vitae. In eu nisl libero. Donec accumsan ac justo sed dictum. Morbi molestie ac arcu vitae commodo. Integer vel nunc lorem. Praesent eu feugiat tortor. Nunc malesuada quis augue ut sollicitudin. Suspendisse lacinia risus ac augue mollis, vitae ultricies purus facilisis.

            Curabitur accumsan enim ut lacus ornare, id ultrices quam maximus. Cras id fermentum odio. Suspendisse luctus maximus sapien, eu gravida erat vehicula sit amet. Suspendisse rhoncus ligula in metus posuere dapibus sit amet quis lorem. Nullam viverra ipsum eget felis consectetur vestibulum. Praesent sagittis porta convallis. Duis pretium vestibulum risus quis sollicitudin. Praesent quis dolor ornare, commodo metus in, venenatis nisl. Donec convallis porta convallis. Nunc aliquam venenatis elit at fringilla. Donec dapibus lobortis vestibulum. Nam ac lacus vitae nunc vulputate congue.
            
            Maecenas sed tortor quis mauris aliquet auctor. Aliquam malesuada nisl vel velit sollicitudin, vel finibus mi scelerisque. Vivamus porta semper laoreet. Integer urna turpis, elementum sit amet commodo ut, sollicitudin in nisl. Cras sed congue metus. Pellentesque varius libero eu arcu vestibulum, id commodo urna feugiat. Phasellus rhoncus in ex vel pretium. Aenean condimentum orci non facilisis faucibus. Sed condimentum velit ac mollis lobortis.
            
            Donec convallis non nibh at tincidunt. In sem urna, scelerisque sit amet erat nec, placerat accumsan sem. Maecenas pretium nisl erat, sed aliquam turpis sagittis et. Integer sodales quam ac odio placerat, nec mollis justo viverra. Pellentesque accumsan nibh in quam pretium, a euismod ex elementum. Donec ut malesuada dui. Sed at dapibus nulla. Donec et dui auctor, sodales mauris in, malesuada magna. Etiam et sem sit amet mi gravida sollicitudin pretium vulputate dui. Ut ac orci eget ipsum fringilla facilisis. Morbi ultrices tortor ac aliquet fringilla. Cras tempor posuere dolor. Pellentesque nulla odio, eleifend et neque a, iaculis facilisis nulla. Ut id leo a turpis fermentum fringilla vitae sit amet tortor. Vestibulum aliquam, ipsum a congue convallis, eros odio bibendum metus, quis ultrices nisi nunc at erat. In bibendum mauris ligula, quis lacinia turpis venenatis quis.')
            ->setCreationDate(new \DateTime('2021-05-22 20:00'))
            ->setPublicationDate(new \DateTime('2021-05-22 20:00'));
        $manager->persist($article4);

        $article5 = new Article();
        $article5->setTitle('Article5')
            ->setPicture('https://www.fnacspectacles.com/static/0/visuel/310/429/47TER-TOUR_4297087678870546068.jpg')
            ->setContent('Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed pulvinar malesuada sapien, vitae mattis libero feugiat vitae. Phasellus semper lorem sit amet varius facilisis. Vestibulum ac bibendum lacus. Duis at risus dui. Cras in viverra ex. Sed consequat, ligula sed rhoncus feugiat, velit nibh pretium ligula, non interdum purus enim eget quam. Etiam et dictum nisi. Donec quis orci libero. Praesent iaculis nunc urna, elementum feugiat diam maximus vitae. In eu nisl libero. Donec accumsan ac justo sed dictum. Morbi molestie ac arcu vitae commodo. Integer vel nunc lorem. Praesent eu feugiat tortor. Nunc malesuada quis augue ut sollicitudin. Suspendisse lacinia risus ac augue mollis, vitae ultricies purus facilisis.

            Curabitur accumsan enim ut lacus ornare, id ultrices quam maximus. Cras id fermentum odio. Suspendisse luctus maximus sapien, eu gravida erat vehicula sit amet. Suspendisse rhoncus ligula in metus posuere dapibus sit amet quis lorem. Nullam viverra ipsum eget felis consectetur vestibulum. Praesent sagittis porta convallis. Duis pretium vestibulum risus quis sollicitudin. Praesent quis dolor ornare, commodo metus in, venenatis nisl. Donec convallis porta convallis. Nunc aliquam venenatis elit at fringilla. Donec dapibus lobortis vestibulum. Nam ac lacus vitae nunc vulputate congue.
            
            Maecenas sed tortor quis mauris aliquet auctor. Aliquam malesuada nisl vel velit sollicitudin, vel finibus mi scelerisque. Vivamus porta semper laoreet. Integer urna turpis, elementum sit amet commodo ut, sollicitudin in nisl. Cras sed congue metus. Pellentesque varius libero eu arcu vestibulum, id commodo urna feugiat. Phasellus rhoncus in ex vel pretium. Aenean condimentum orci non facilisis faucibus. Sed condimentum velit ac mollis lobortis.
            
            Donec convallis non nibh at tincidunt. In sem urna, scelerisque sit amet erat nec, placerat accumsan sem. Maecenas pretium nisl erat, sed aliquam turpis sagittis et. Integer sodales quam ac odio placerat, nec mollis justo viverra. Pellentesque accumsan nibh in quam pretium, a euismod ex elementum. Donec ut malesuada dui. Sed at dapibus nulla. Donec et dui auctor, sodales mauris in, malesuada magna. Etiam et sem sit amet mi gravida sollicitudin pretium vulputate dui. Ut ac orci eget ipsum fringilla facilisis. Morbi ultrices tortor ac aliquet fringilla. Cras tempor posuere dolor. Pellentesque nulla odio, eleifend et neque a, iaculis facilisis nulla. Ut id leo a turpis fermentum fringilla vitae sit amet tortor. Vestibulum aliquam, ipsum a congue convallis, eros odio bibendum metus, quis ultrices nisi nunc at erat. In bibendum mauris ligula, quis lacinia turpis venenatis quis.')
            ->setCreationDate(new \DateTime('2021-05-22 20:00'))
            ->setPublicationDate(new \DateTime('2021-05-22 20:00'));
        $manager->persist($article5);

        $manager->flush();
    }
}
